<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use Illuminate\Http\Request;
use Validator;

class BlogPostController extends Controller
{
    // Apply 'auth:api' middleware to all routes except 'index' and 'show'
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Get a paginated list of blog posts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retrieve blog posts, ordered by publication date in descending order
        $posts = BlogPost::orderBy('publication_date', 'desc')->paginate(10);

        return response()->json($posts);
    }

    /**
     * Create a new blog post for the authenticated user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();

        // Validate the request data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|between:5,100',
            'content' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        // Create a new instance of the BlogPost model
        $post = new BlogPost();
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->publication_date = now();

        // Associate the blog post with the authenticated user
        $post->user()->associate($user);
        $post->save();

        return response()->json($post, 201);
    }

    /**
     * Get the specified blog post by ID.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Find the blog post by ID
        $post = BlogPost::find($id);

        if ($post) {
            return response()->json($post);
        }

        return response()->json([
            'code' => 404,
            'message' => "Post not found"
        ], 404);
    }

    /**
     * Update the specified blog post for the authenticated user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $request->user();
        // Find the blog post by ID and ensure it belongs to the authenticated user
        $post = BlogPost::where('user_id', $user->id)->find($id);

        if ($post) {
           // Validate the request data
            $validator = Validator::make($request->all(), [
              'title' => 'required|string|between:2,100',
              'content' => 'required|string',
          ]);

          if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
          }
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();

            return response()->json($post);
        }

        return response()->json([
            'code' => 404,
            'message' => "Post not found"
        ], 404);
    }

    /**
     * Delete the specified blog post for the authenticated user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = $request->user();

        // Find the blog post by ID and ensure it belongs to the authenticated user
        $post = BlogPost::where('user_id', $user->id)->find($id);

        if ($post) {
            $post->delete();
            return response()->json([
                'code' => 200,
                'message' => 'Post deleted'
            ], 200);
        }

        return response()->json([
            'code' => 404,
            'message' => "Post not found"
        ], 404);
    }
}
